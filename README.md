# README #

### TITLE
Title: Delivery Route API

### CONTENT
Content: I created four folders API,Core,Includes,Sql and "read.me" file.The "API" folder display Create , Read, Update ,Delete and reports. The "Core" folder is for Queries and for path and configure to easily so access.The "Includes" folder is to create a connection to the database.And lastly the Sql folder, is for database file.I created database that contains three tables named Users,Routes and Delivery. users table, it store the basic information of the users.Delivery table, it store the basic information of the delivery.Routes table, is for destination of delivery.Delivery table, to finalize shipping  report.

### API
* the sample data is the specific file inside of commit

### CORE
* it's all about queries and connections

### INCLUDES
* it's all about config or database connection

### SQL
* database file