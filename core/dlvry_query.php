<?php

    class dlvry{
        public  $conn;
        public  $table = 'delivery';
        public  $user_id;
        public  $route_id;          

        public function __construct($db){
            $this->conn = $db;
        }

        // view data
        public function read(){
            $query = "SELECT *
            FROM routes";
            $statement = $this->conn->prepare($query);
            $statement->execute();
            return $statement;
        }


        // read by id
        public function read_single(){
            $query = "SELECT * FROM routes WHERE r_from=? and r_to=?";
            // SELECT * FROM routes WHERE id= 1 and r_from = 'A'

            $statement = $this->conn->prepare($query);
            // before  execute bind the param first
            $statement->bindParam(1,$this->r_from);
            $statement->bindParam(2,$this->r_to);
            $statement->execute();
            $row = $statement->fetch(PDO::FETCH_ASSOC);

            $this->id  = $row['id'];
            $this->r_from  = $row['r_from'];
            $this->r_to    = $row['r_to'];
            $this->r_time  = $row['r_time'];
            $this->r_cost  = $row['r_cost'];
        }

         // read by route
         public function read_path(){
            $query = "SELECT origin.r_from as origin_from, origin.r_to as origin_to, origin.r_time as origin_time,origin.r_cost as origin_cost,
            destination.r_from as destination_from, destination.r_to as destination_to, destination.r_time as destination_time,destination.r_cost as destination_cost,
            originR_id,destinationR_id
           FROM delivery as drvry left join routes as origin on drvry.originR_id =origin.id left join routes as destination on drvry.destinationR_id = destination.id WHERE originR_id =? and destinationR_id=?";


            // $query = "SELECT * FROM delivery  WHERE originR_id =? and destinationR_id=?";

            $statement = $this->conn->prepare($query);
            // before  execute bind the param first
            $statement->bindParam(1,$this->originR_id);
            $statement->bindParam(2,$this->destinationR_id);
            $statement->execute();
            $row = $statement->fetch(PDO::FETCH_ASSOC);

            
            $this->origin_from  = $row['origin_from'];
            $this->origin_to    = $row['origin_to'];
            $this->destination_from  = $row['destination_from'];
            $this->destination_to  = $row['destination_to'];
            // time and cost
            $this->origin_time  = $row['origin_time'];
            $this->origin_cost  = $row['origin_cost'];
            $this->destination_time  = $row['destination_time'];
            $this->destination_cost  = $row['destination_cost'];
        }

        //create data
        public function create(){
            $query ='INSERT INTO '.$this->table.' SET 
                                        originR_id = :originR_id,
                                        destinationR_id = :destinationR_id
                                        ';                   
            // prepare            
            $statement = $this->conn->prepare($query);
            
            // for Route CHECKER                       

            // clean data            
            $this->originR_id          =htmlspecialchars(strip_tags($this->originR_id));
            $this->destinationR_id          =htmlspecialchars(strip_tags($this->destinationR_id));
                
            // $d_cost = floatval($this->d_cost)+floatval($route_cost);
            // echo $route_cost;
            // bind params            
            $statement->bindParam(':originR_id',$this->originR_id);
            $statement->bindParam(':destinationR_id',$this->destinationR_id);
        
            if($statement-> execute())
            {
                return true;
            }
            printf("error %s. \n",$statement->error);
            return false;

        }
    }

?>

