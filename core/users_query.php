<?php

    class Post{
        public  $conn;
        public  $table = 'users';
        public  $id;
        public  $name;
        public  $type;    

        public function __construct($db){
            $this->conn = $db;
        }

        // view data
        public function read(){
            $query = "SELECT *   FROM users";
            $statement = $this->conn->prepare($query);
            $statement->execute();
            return $statement;
        }


        // create user
        public function create(){
            $query ='INSERT INTO '.$this->table.' SET name = :name, type = :type';

            // prepare
            $statement = $this->conn->prepare($query);

            // clean data
            $this->name             =htmlspecialchars(strip_tags($this->name));
            $this->type          =htmlspecialchars(strip_tags($this->type));

            // bind params
            $statement->bindParam(':name',$this->name);
            $statement->bindParam(':type',$this->type);

            if($statement-> execute())
            {
                return true;
            }
            printf("error %s. \n",$statement->error);
            return false;

        }

         //Delete Data
         public function delete(){
            $query ='DELETE FROM '.$this->table.' WHERE id =:id';

            // prepare
            $statement = $this->conn->prepare($query);

            // clean data
            $this->id  =htmlspecialchars(strip_tags($this->id));

            // bind params
            $statement->bindParam(':id',$this->id);

            if($statement->execute())
            {
                return true;
            }
            printf("error %s. \n",$statement->error);
            return false;
        }
    }

?>
