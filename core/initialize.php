<?php
    
    defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
    defined('SITE_ROOT') ? null : define('SITE_ROOT', DS. 'xampp'. DS. 'htdocs'.DS.'pathlocator');

    // xampp/www/DLVRYSRVC_API/includes
    defined('INC_PATH') ? null : define('INC_PATH',SITE_ROOT.DS.'includes');

    // xampp/www/DLVRYSRVC_API/core
    defined('CORE_PATH') ? null : define('CORE_PATH',SITE_ROOT.DS.'core');

    // to access config file
    require_once(INC_PATH.DS."config.php");

    // to access query file
  //  require_once(CORE_PATH.DS."users_query.php");     
    require_once(CORE_PATH.DS."dlvry_query.php");
    require_once(CORE_PATH.DS."route_query.php");
    require_once(CORE_PATH.DS."users_query.php");
?>  
