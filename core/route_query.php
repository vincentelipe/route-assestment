<?php

    class Route{
        public  $conn;
        public  $table = 'routes';
        public  $origin;
        public  $destination;
        public  $r_cost;

        public function __construct($db){
            $this->conn = $db;
        }

        // view data
        public function read(){
            $query = "SELECT * FROM routes";
            $statement = $this->conn->prepare($query);
            $statement->execute();
            return $statement;
        }

        // create data
        public function create(){
            $query ='INSERT INTO '.$this->table.' SET r_from = :r_from, r_to = :r_to,  r_time = :r_time,  r_cost = :r_cost';                        
            // prepare
            $statement = $this->conn->prepare($query);

            // clean data
            $this->r_from              =htmlspecialchars(strip_tags($this->r_from));
            $this->r_to         =htmlspecialchars(strip_tags($this->r_to));
            $this->r_time         =htmlspecialchars(strip_tags($this->r_time));
            $this->r_cost                =htmlspecialchars(strip_tags($this->r_cost));

            // bind params
            $statement->bindParam(':r_from',$this->r_from);
            $statement->bindParam(':r_to',$this->r_to);
            $statement->bindParam(':r_time',$this->r_time);
            $statement->bindParam(':r_cost',$this->r_cost);
            
            if($statement-> execute())
            {
                return true;
            }
            printf("error %s. \n",$statement->error);
            return false;
        }

          //Update Data
          public function update(){
            $query ='UPDATE '.$this->table.' SET r_from = :r_from, r_to = :r_to,r_time = :r_time,r_cost = :r_cost WHERE id =:id';

            // prepare
            $statement = $this->conn->prepare($query);

            // clean data
            $this->id  =htmlspecialchars(strip_tags($this->id));
            $this->r_from  =htmlspecialchars(strip_tags($this->r_from));
            $this->r_to  =htmlspecialchars(strip_tags($this->r_to));
            $this->r_time  =htmlspecialchars(strip_tags($this->r_time));
            $this->r_cost  =htmlspecialchars(strip_tags($this->r_cost));

            // bind params
            $statement->bindParam(':id',$this->id);
            $statement->bindParam(':r_from',$this->r_from);
            $statement->bindParam(':r_to',$this->r_to);
            $statement->bindParam(':r_time',$this->r_time);
            $statement->bindParam(':r_cost',$this->r_cost);

            if($statement-> execute())
            {
                return true;
            }
            printf("error %s. \n",$statement->error);
            return false;
        }
        
        //Delete Data
        public function delete(){
            $query ='DELETE FROM '.$this->table.' WHERE id =:id';

            // prepare
            $statement = $this->conn->prepare($query);

            // clean data
            $this->id  =htmlspecialchars(strip_tags($this->id));

            // bind params
            $statement->bindParam(':id',$this->id);

            if($statement->execute())
            {
                return true;
            }
            printf("error %s. \n",$statement->error);
            return false;
        }
    }

?>
