<?php 
    $servername = 'localhost';
    $username = "root";
    $password = "";
    $dbname = "pathlocator";

    // db connection    
    try {
        $db = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
        // set the PDO error mode to exception
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "Connected successfully"; 
        }
    catch(PDOException $e)
        {
        echo "Connection failed: " . $e->getMessage();
        }

        define('APP_NAME','PHP REST API');
    
?>  