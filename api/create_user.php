<?php
    //  http://localhost/pathlocator/api/create_user.php
    
    // sample data
    // {
    //     "name":"vincent",
    //     "type":"admin"
    // }
    
    header('Access-Control-Allow-Origin: * ');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST'); 
    header('Access-Control-Allow-Headers:Access-Control-Allow-Headers:,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

    // include for access files
    include_once('../core/initialize.php');

    $post = new Post($db);
    
    // get all value you input in postman
    $data = json_decode(file_get_contents("php://input"));
    
    $post->name = $data->name;
    $post->type = $data->type;
    
    if($post->create()) 
    {
        echo json_encode(
            array('message'=> 'User Created.')
        );
    }
    else{
        echo json_encode(
            array('message'=> 'User Not  Created.')
        );
    }
?>