<?php
    // http://localhost/routeAPI/api/dlvry_reports.php
    
    header('Access-Control-Allow-Origin: * ');
    header('Content-Type: application/json');

    include_once('../core/initialize.php');

    $post = new dlvry($db);

    $result = $post->read();

    $num = $result->rowCount();

    if($num>0){
        $post_arr = array();
        $post_arr['data'] = array();
        echo "THIS IS DELIVERY REPORT\n";
        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);            
            
            $post_item = array(
                'origin' => $r_from,
                'point' => $r_to,
                'destination' => $destination,
                'time ' => $d_time+$r_time,
                'cost' => $d_cost+$d_cost
            );
            array_push($post_arr['data'], $post_item);
        }

        //convert json and display        
        echo json_encode($post_arr);        
    }
    else{
        echo json_encode(array('message' => 'No data Found'));
    }
?>