<?php
    // http://localhost/routeAPI/api/read_route.php
    
    header('Access-Control-Allow-Origin: * ');
    header('Content-Type: application/json');

    include_once('../core/initialize.php');

    $post = new Route($db);

    $result = $post->read();

    $num = $result->rowCount();

    if($num>0){
        $post_arr = array();
        $post_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)){
            extract($row);            
            $post_item = array(
                'id' => $id,
                'point' => $r_from,
                'destination' => $r_to,
                'cost' =>$r_cost,
                'time' => $r_time
            );
            array_push($post_arr['data'], $post_item);
        }

        //convert json and display        
        echo json_encode($post_arr);        
    }
    else{
        echo json_encode(array('message' => 'No data Found'));
    }
?>