<?php
    //  http://localhost/pathlocator/api/read_specificID.php?origin=A&destination=B

    header('Access-Control-Allow-Origin: * ');
    header('Content-Type: application/json');

    include_once('../core/initialize.php');

    $post = new dlvry($db);

   $post->r_from =isset($_GET['origin']) ? $_GET['origin'] :die();
   $post->r_to =isset($_GET['destination']) ? $_GET['destination'] :die();
   $post->read_single();

   $post_arr = array(
       'id' => $post->id,
       'from' => $post->r_from,
       'to' => $post->r_to,
       'time' => $post->r_time,
       'cost' => $post->r_cost
   );
   if($post->id == true && $post->r_from == true){
       print_r(json_encode($post_arr));
   }
   else{
    echo json_encode(array('message' => 'Invalid data'));
   }
?>