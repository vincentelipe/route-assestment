<?php
    //  http://localhost/pathlocator/api/update_route.php
    // sample data
    // {
    //     "id":"5",
    //     "r_from":"Molo Plazaa",
    //     "r_to":"UP visayas",
    //     "r_time":"12",
    //      "r_cost":"12"
    //   }

    header('Access-Control-Allow-Origin: * ');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: PUT'); 
    header('Access-Control-Allow-Headers:Access-Control-Allow-Headers:,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

    include_once('../core/initialize.php');

    $post = new Route($db);

    $data = json_decode(file_get_contents("php://input"));
    
    $post->id = $data->id;
    $post->r_from = $data->r_from;
    $post->r_to = $data->r_to;
    $post->r_time = $data->r_time;
    $post->r_cost = $data->r_cost;
    
    if($post->update()) 
    {
        echo json_encode(
            array('message'=> 'Route Updated.')
        );
    }
    else{
        echo json_encode(
            array('message'=> 'Route Not  Updated.')
        );
    }
?>