<?php
    //  http://localhost/pathlocator/api/read_path.php?origin=A&destination=B

    header('Access-Control-Allow-Origin: * ');
    header('Content-Type: application/json');

    include_once('../core/initialize.php');

    $post = new dlvry($db);

   $post->originR_id =isset($_GET['origin']) ? $_GET['origin'] :die();
   $post->destinationR_id =isset($_GET['destination']) ? $_GET['destination'] :die();
   $post->read_path();

   $post_arr = array(
       'Origin' => $post->origin_from,
       'Middleman' => $post->origin_to,
       'Destination' => $post->destination_to,
       'Total time' => $post->origin_time +$post->destination_time,
       'Total cost' => $post->origin_cost +$post->destination_cost
   );
   if($post->originR_id == true && $post->destinationR_id == true){
        
       print_r(json_encode($post_arr));
   }
//    elseif($post->originR_id == true || $post->destinationR_id == false){
//     echo json_encode(array('message' => 'Invalid data'));
//    }
   else{
    echo json_encode(array('message' => 'Invalid data'));
   }
?>