<?php
    //  http://localhost/pathlocator/api/create_route.php
    
    // sample data
    // {
    //     "r_from":"J",
    //     "r_to":"K",
    //     "r_time":"22",
    //      "r_cost":"22"
    //   }
    
    header('Access-Control-Allow-Origin: * ');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST'); 
    header('Access-Control-Allow-Headers:Access-Control-Allow-Headers:,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

    // include for access files
    include_once('../core/initialize.php');

    $post = new Route($db);
    
    // get all value you input in postman
    $data = json_decode(file_get_contents("php://input"));
    
    $post->r_from = $data->r_from;
    $post->r_to = $data->r_to;
    $post->r_time = $data->r_time;
    $post->r_cost = $data->r_cost;

    if($post->create()) 
    {
        echo json_encode(
            array('message'=> 'Route Created.')
        );
    }
    else{
        echo json_encode(
            array('message'=> 'Route Not  Created.')
        );
    }
?>
