<?php
    // http://localhost/pathlocator/api/create_delivery.php
    
    // sample data
    // {
    //     "from_loc":" FROM VALUE ",
    //     "to_loc":" TO VALUE ",
    //     "time":"12",
    //      "cost":"12"
    //   }
    
    header('Access-Control-Allow-Origin: * ');
    header('Content-Type: application/json');
    header('Access-Control-Allow-Methods: POST'); 
    header('Access-Control-Allow-Headers:Access-Control-Allow-Headers:,Content-Type,Access-Control-Allow-Methods,Authorization,X-Requested-With');

    // include for access files
    include_once('../core/initialize.php');

    $post = new dlvry($db);
    
    // get all value you input in postman
    $data = json_decode(file_get_contents("php://input"));
    
    $post->originR_id = $data->originR_id;
    $post->destinationR_id = $data->destinationR_id;

    if($post->create()) 
    {
        echo json_encode(
            array('message'=> 'delivery Created.')
        );
    }
    else{
        echo json_encode(
            array('message'=> 'delivery Not  Created.')
        );
    }
?>
